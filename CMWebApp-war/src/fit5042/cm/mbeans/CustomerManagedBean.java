package fit5042.cm.mbeans;

import fit5042.cm.repository.CustomerRepository;
import fit5042.cm.repository.entities.Address;
import fit5042.cm.repository.entities.Contact;
import fit5042.cm.repository.entities.Customer;
import fit5042.cm.repository.entities.IndustryType;
import fit5042.cm.repository.entities.AppUser;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Sunpreet Kaur
 * 
 */
@ManagedBean(name = "customerManagedBean")
@SessionScoped

public class CustomerManagedBean implements Serializable {

    @EJB
   CustomerRepository  customerRepository;

    
    public CustomerManagedBean() {
    }

    public List<Customer> getAllCustomers() {
        try {
            List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
   public void addCustomer(Customer customer) 
    {
        try {
             customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   
    public Customer searchCustomerById(int id)
    {
        try {
            return customerRepository.searchCustomerById(id);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
   
    public List<Customer> searchCustomerByName(String customerName)
    {
        try {
            return customerRepository.searchCustomerByName(customerName);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    public List<IndustryType> getAllIndustryType() {
    	 try {
             return customerRepository.getAllIndustryType();
         } catch (Exception ex) {
             Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
         }
       
        return null;
    }


    public void removeCustomer(int customerId) 
    {
        try {
            customerRepository.removeCustomer(customerId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editCustomer(Customer customer)
    {
        try {
          
            
            customerRepository.editCustomer(customer);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Customer> searchCustomerByNoOfProductPurchased(int noOfProductsPurchased)
    {
        try {
            return customerRepository.searchCustomerByNoOfProductPurchased(noOfProductsPurchased);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public void addCustomer(fit5042.cm.controllers.Customer localCustomer) {
       
    	Customer customer = convertCustomerToEntity(localCustomer);
    	
    	

        try {
            customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    private Customer convertCustomerToEntity(fit5042.cm.controllers.Customer localCustomer) {
    	Customer customer = new Customer(); 
    	
    	customer.setCustomerId(localCustomer.getCustomerId());
        customer.setName(localCustomer.getName());
        customer.setPhoneNo(localCustomer.getPhoneNo());
        customer.setEmail(localCustomer.getEmail());
        customer.setNoOfProductsPurchased(localCustomer.getNoOfProductsPurchased());
        customer.setRegisteredOn(localCustomer.getRegisteredOn());
        customer.setRegisteredBy(localCustomer.getRegisteredBy());

        customer.setIndustryType(localCustomer.getIndustryType());
        customer.setCustomerAssignedTo(localCustomer.getCustomerAssignedTo());

        String streetNumber = localCustomer.getStreetNumber();
        String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        customer.setAddress(address);
      
        customer.setIndustryTypeName(localCustomer.getIndustryTypeName());
     
         customer.setCustomerAssignee(localCustomer.getCustomerAssignee());
         return customer;
    }
  

}
