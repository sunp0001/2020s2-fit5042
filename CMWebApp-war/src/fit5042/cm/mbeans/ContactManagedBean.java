package fit5042.cm.mbeans;

import fit5042.cm.repository.ContactRepository;
import fit5042.cm.repository.CustomerRepository;
import fit5042.cm.repository.entities.Address;
import fit5042.cm.repository.entities.Contact;
import fit5042.cm.repository.entities.Customer;
//import fit5042.cm.repository.entities.IndustryType;
import fit5042.cm.repository.entities.AppUser;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Sunpreet Kaur
 * 
 */
@ManagedBean(name = "contactManagedBean")
@SessionScoped

public class ContactManagedBean implements Serializable {

    @EJB
   ContactRepository  contactRepository;
    CustomerRepository customerRepository;

    
    public ContactManagedBean() {
    }

    public List<Contact> getAllContacts() {
        try {
            List<Contact> contacts = contactRepository.getAllContacts();
            return contacts;
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void addContact(Contact contact) 
    {
        try {
        	contactRepository.addContact(contact);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   
    public Contact searchContactById(int id)
    {
        try {
            return contactRepository.searchContactById(id);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
   
    public List<Contact> searchContactByName(String contactName)
    {
        try {
            return contactRepository.searchContactByName(contactName);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    public Set<Contact> searchContactByCustomerId(int contactCustomerId) 
    {
        try {
        	  for (Customer customer : customerRepository.getAllCustomers()) {
                  if (customer.getCustomerId() == contactCustomerId) {
                      return contactRepository.searchContactByCustomer(customer);
                  }
              }
           
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    

    public void removeContact(int contactId) 
    {
        try {
        	contactRepository.removeContact(contactId);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editContact(Contact contact)
    {
        try {
          
            contactRepository.editContact(contact);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

  

    public void addContact(fit5042.cm.controllers.Contact localContact) {
       
    	Contact contact = convertContactToEntity(localContact);

        try {
            contactRepository.addContact(contact);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Set<Contact> searchContactByCustomer(Customer customer) throws Exception {
    	
    	
    	try {
            return contactRepository.searchContactByCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
   
    private Contact convertContactToEntity(fit5042.cm.controllers.Contact localContact) {
    	Contact contact = new Contact(); //entity
      	contact.setContactId(localContact.getContactId());
    	contact.setName(localContact.getName());
    	contact.setPhoneNo(localContact.getPhoneNo());
    	contact.setEmail(localContact.getEmail());
    	contact.setRole(localContact.getRole());
    	contact.setRegisteredOn(localContact.getRegisteredOn());
    	contact.setRegisteredBy(localContact.getRegisteredBy());
    	contact.setContactCustomer(localContact.getContactCustomer());
    	contact.setContactCustomerId(localContact.getContactCustomerId());
 
        return contact;
    
    }
    

}