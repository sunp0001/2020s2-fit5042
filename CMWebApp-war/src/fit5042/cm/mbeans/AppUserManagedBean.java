package fit5042.cm.mbeans;

import fit5042.cm.repository.AppUserRepository;

import fit5042.cm.repository.entities.AppUser;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Sunpreet Kaur
 * 
 */
@ManagedBean(name = "appUserManagedBean")
@SessionScoped

public class AppUserManagedBean implements Serializable {

    @EJB
    AppUserRepository  appUserRepository;

  
    public AppUserManagedBean() {
    }

    public List<AppUser> getAllAppUsers() {
        try {
            List<AppUser> appUsers = appUserRepository.getAllAppUsers();
            return appUsers;
        } catch (Exception ex) {
            Logger.getLogger(AppUserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void addAppUser(AppUser appUser) 
    {
        try {
        	appUserRepository.addAppUser(appUser);
        } catch (Exception ex) {
            Logger.getLogger(AppUserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   
    public AppUser searchAppUserById(int id)
    {
        try {
            return appUserRepository.searchAppUserById(id);
        } catch (Exception ex) {
            Logger.getLogger(AppUserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
   
    public AppUser searchAppUserByName(String userName)
    {
        try {
            return appUserRepository.searchAppUserByName(userName);
        } catch (Exception ex) {
            Logger.getLogger(AppUserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }


    public void removeAppUser(int userId) 
    {
        try {
            appUserRepository.removeAppUser(userId);
        } catch (Exception ex) {
            Logger.getLogger(AppUserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editAppUser(AppUser appUser)
    {
        try {
       
            
            appUserRepository.editAppUser(appUser);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(AppUserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void addAppUser(fit5042.cm.controllers.AppUser localAppUser) {
       
    	AppUser appUser = convertUserToEntity(localAppUser);

        try {
        	appUserRepository.addAppUser(appUser);
        } catch (Exception ex) {
            Logger.getLogger(AppUserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    private AppUser convertUserToEntity(fit5042.cm.controllers.AppUser localAppUser) {
    	AppUser appUser = new AppUser(); 
        
    	appUser.setId(localAppUser.getId());
    	appUser.setName(localAppUser.getName());
    	appUser.setPhoneNo(localAppUser.getPhoneNo());
    	appUser.setUsername(localAppUser.getUsername());
    	appUser.setPassword(localAppUser.getPassword());       
        return appUser;
    }
    

}

