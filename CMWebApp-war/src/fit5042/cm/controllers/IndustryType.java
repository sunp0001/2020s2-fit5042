package fit5042.cm.controllers;

import java.io.Serializable;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;


import fit5042.cm.repository.entities.Customer;

/**
 *
 * @author Sunpreet Kaur
 */
@RequestScoped
@Named(value = "industryType")
public class IndustryType implements Serializable {

	private int industryTypeId;
	private String industryTypeName;

	private Set<Customer> customers;

	public IndustryType() {

	}

	public IndustryType(int industryTypeId, String industryTypeName) {
		super();
		this.industryTypeId = industryTypeId;
		this.industryTypeName = industryTypeName;
	}

	public int getIndustryTypeId() {
		return industryTypeId;
	}

	public void setIndustryTypeId(int industryTypeId) {
		this.industryTypeId = industryTypeId;
	}

	public String getIndustryTypeName() {
		return industryTypeName;
	}

	public void setIndustryTypeName(String industryTypeName) {
		this.industryTypeName = industryTypeName;
	}

	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setContacts(Set<Customer> customers) {
		this.customers = customers;
	}

	@Override
	public String toString() {
		return "IndustryType [industryTypeId=" + industryTypeId + ", industryTypeName=" + industryTypeName
				+ ", customers=" + customers + "]";
	}

}
