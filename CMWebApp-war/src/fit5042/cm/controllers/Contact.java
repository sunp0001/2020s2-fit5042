package fit5042.cm.controllers;

import fit5042.cm.repository.entities.Address;
import fit5042.cm.repository.entities.Customer;
//import fit5042.cm.repository.entities.IndustryType;
import fit5042.cm.repository.entities.AppUser;

import java.io.Serializable;
import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Sunpreet Kaur
 */
@RequestScoped
@Named(value = "contact")
public class Contact implements Serializable {

	private int contactId;
	private String name;
	private String phoneNo;
	private String email;
	private String role;
	private String registeredBy;
	private Date registeredOn;

	private Customer contactCustomer;
	private int contactCustomerId;


	public Contact() {
	}

	public int getContactCustomerId() {
		return contactCustomerId;
	}

	public void setContactCustomerId(int contactCustomerId) {
		this.contactCustomerId = contactCustomerId;
	}

	public Contact(int contactId, String name, String phoneNo, String email, String role, Date registeredOn,
			String registeredBy, Customer contactCustomer, int contactCustomerId) {
		super();
		this.contactId = contactId;
		this.name = name;
		this.phoneNo = phoneNo;
		this.email = email;
		this.role = role;
		this.registeredOn = registeredOn;
		this.registeredBy = registeredBy;
		this.contactCustomer = contactCustomer;
		this.contactCustomerId = contactCustomerId;
	}
	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date getRegisteredOn() {
		return registeredOn;
	}

	public void setRegisteredOn(Date registeredOn) {
		this.registeredOn = registeredOn;
	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public Customer getContactCustomer() {
		// TODO Auto-generated method stub
		return contactCustomer;
	}

	public void setContactCustomer(Customer contactCustomer) {
		this.contactCustomer = contactCustomer;
	}
	

	@Override
	public String toString() {
		return "Contact [contactId=" + contactId + ", name=" + name + ", phoneNo=" + phoneNo + ", email=" + email
				+ ", role=" + role + ", registeredBy=" + registeredBy + ", registeredOn=" + registeredOn
				+ ", contactCustomer=" + contactCustomer + "]";
	}
	
	

	
	
	

	

	

}
