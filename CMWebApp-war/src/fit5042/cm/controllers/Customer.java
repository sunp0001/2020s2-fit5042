package fit5042.cm.controllers;

import fit5042.cm.repository.entities.Address;
import fit5042.cm.repository.entities.Contact;
import fit5042.cm.repository.entities.IndustryType;
import fit5042.cm.repository.entities.AppUser;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Sunpreet Kaur
 */
@RequestScoped
@Named(value = "customer")
public class Customer implements Serializable {

	private int customerId;
	private String name;
	private String phoneNo;
	private String email;
	private int noOfProductsPurchased;
	private Date registeredOn;
	private String registeredBy;

	private Address address;

	private String streetNumber;
	private String streetAddress;
	private String suburb;
	private String postcode;
	private String state;

	private Set<Contact> contacts;

	private IndustryType industryType;
	
	private int industryTypeId;
	private String industryTypeName;

	private AppUser customerAssignedTo;
	private String customerAssignee;
	private int userId;


	public Customer() {

	}
	



	public Customer(int customerId, String name, String phoneNo, String email, int noOfProductsPurchased,
			Date registeredOn, String registeredBy, Address address,IndustryType industryType, AppUser customerAssignedTo, String industryTypeName,String customerAssignee) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.phoneNo = phoneNo;
		this.email = email;
		this.noOfProductsPurchased = noOfProductsPurchased;
		this.registeredOn = registeredOn;
		this.registeredBy = registeredBy;
		this.address = address;
		this.industryType = industryType;
		this.customerAssignedTo = customerAssignedTo;
		this.industryTypeName = industryTypeName;
		this.customerAssignee = customerAssignee;
	}


	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getNoOfProductsPurchased() {
		return noOfProductsPurchased;
	}

	public void setNoOfProductsPurchased(int noOfProductsPurchased) {
		this.noOfProductsPurchased = noOfProductsPurchased;
	}

	public Date getRegisteredOn() {
		return registeredOn;
	}

	public void setRegisteredOn(Date registeredOn) {
		this.registeredOn = java.util.Calendar.getInstance().getTime();
	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public IndustryType getIndustryType() {
		return industryType;
	}

	public void setIndustryType(IndustryType industryType) {
		this.industryType = industryType;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public AppUser getCustomerAssignedTo() {
		return customerAssignedTo;
	}

	public void setCustomerAssignedTo(AppUser customerAssignedTo) {
		this.customerAssignedTo = customerAssignedTo;
	}

    public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getIndustryTypeId() {
		return industryTypeId;
	}

	public void setIndustryTypeId(int industryTypeId) {
		this.industryTypeId = industryTypeId;
	}

	public String getIndustryTypeName() {
		return industryTypeName;
	}

	public void setIndustryTypeName(String industryTypeName) {
		this.industryTypeName = industryTypeName;
	}

	

    public int getUserId() {
		// TODO Auto-generated method stub
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}


	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", name=" + name + ", phoneNo=" + phoneNo + ", email=" + email
				+ ", noOfProductsPurchased=" + noOfProductsPurchased + ", registeredOn=" + registeredOn
				+ ", registeredBy=" + registeredBy + ", address=" + address + ", industryType=" + industryType 
				+ ", customerAssignedTo=" + customerAssignedTo + "]";
	}

	public String getCustomerAssignee() {
		return customerAssignee;
	}

	public void setCustomerAssignee(String customerAssignee) {
		this.customerAssignee = customerAssignee;
	}

	

}
