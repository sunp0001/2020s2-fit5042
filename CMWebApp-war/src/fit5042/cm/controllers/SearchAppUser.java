package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;



/**
 *
 * @author Sunpreet Kaur */
@RequestScoped
@Named("searchAppUser")
public class SearchAppUser {
    private AppUser appUser;
    
    AppUserApplication userApp;
    
    private int searchByInt;
    private String searchByString;

    public AppUserApplication getApp() {
        return userApp;
    }

    public void setApp(AppUserApplication userApp) {
        this.userApp = userApp;
    }

    public String getSearchByString() {
        return searchByString;
    }

    public void setSearchByString(String searchByString) {
        this.searchByString = searchByString;
    }

    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }

    
    public void setAppUser(AppUser appUser){
        this.appUser = appUser;
    }
    
    public AppUser getUser(){
        return appUser;
    }
    
    public SearchAppUser() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        userApp = (AppUserApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "appUserApplication");
        
        userApp.updateUserList();
    }

   
    public void searchUserById(int userId) 
    {
       try
       {
       
    	   userApp.searchUserById(userId);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchUserByName(String nameOfUser) 
    {
       try
       {
           
    	   userApp.searchUserByName(nameOfUser);
       }
       catch (Exception ex)
       {
           
       }
    }

    
    public void searchAll() 
    {
       try
       {
            
    	   userApp.searchAll();
       }
       catch (Exception ex)
       {
           
       }
    }
    
}

