package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.cm.mbeans.IndustryTypeManagedBean;
import javax.faces.bean.ManagedProperty;



/**
 *
 * @author Sunpreet Kaur
 */
@RequestScoped
@Named("removeIndustryType")
public class RemoveIndustryType {
   @ManagedProperty(value="#{industryTypeManagedBean}") 
   IndustryTypeManagedBean industryTypeManagedBean;
    
    private boolean showForm = true;
    private IndustryType industryType;
    IndustryTypeApplication industryTypeApp;
    
    public void setIndustryType(IndustryType industryType){
        this.industryType = industryType;
    }
    
    public IndustryType getIndustryType(){
        return industryType;
    }
    
    public boolean isShowForm() {
        return showForm;
    }

    public RemoveIndustryType() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        industryTypeApp = (IndustryTypeApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "industryTypeApplication");
        
        industryTypeApp.updateIndustryTypeList();
        
      
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        industryTypeManagedBean = (IndustryTypeManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "industryTypeManagedBean");
    }

 
    public void removeIndustryType(int industryTypeId) {
       try
       {
           
    	   industryTypeManagedBean.removeIndustryType(industryTypeId);

    	   industryTypeApp.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Industry Type has been deleted succesfully"));     
       }
       catch (Exception ex)
       {
           
       }
       showForm = true;

    }
 
}


