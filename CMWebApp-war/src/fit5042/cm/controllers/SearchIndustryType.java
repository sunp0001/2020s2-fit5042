package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Sunpreet Kaur */
@RequestScoped
@Named("searchIndustryType")
public class SearchIndustryType {
    private IndustryType industryType;
    
    IndustryTypeApplication industryTypeApp;
    
    private int searchByInt;
    private String searchByString;

    public IndustryTypeApplication getApp() {
        return industryTypeApp;
    }

    public void setApp(IndustryTypeApplication industryTypeApp) {
        this.industryTypeApp = industryTypeApp;
    }
 

    public String getSearchByString() {
        return searchByString;
    }

    public void setSearchByString(String searchByString) {
        this.searchByString = searchByString;
    }

    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }
    
    public void setIndustryType(IndustryType industryType){
        this.industryType = industryType;
    }
    
    public IndustryType getIndustryType(){
        return industryType;
    }
    
    public SearchIndustryType() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        industryTypeApp = (IndustryTypeApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "industryTypeApplication");
        
        industryTypeApp.updateIndustryTypeList();
    }

   
    public void searchIndustryTypeById(int industryTypeId) 
    {
       try
       {
            
    	   industryTypeApp.searchIndustryTypeById(industryTypeId);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchIndustryTypeByName(String industryTypeName) 
    {
       try
       {
      
    	   industryTypeApp.searchIndustryTypeByName(industryTypeName);
       }
       catch (Exception ex)
       {
           
       }
    }

    
    public void searchAll() 
    {
       try
       {
      
    	   industryTypeApp.searchAll();
       }
       catch (Exception ex)
       {
           
       }
    }
    
}

