package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.cm.mbeans.CustomerManagedBean;
import javax.faces.bean.ManagedProperty;


/**
 *
 * @author Sunpreet Kaur
 */
@RequestScoped
@Named("removeCustomer")
public class RemoveCustomer {
   @ManagedProperty(value="#{customerManagedBean}") 
   CustomerManagedBean customerManagedBean;
    
    private boolean showForm = true;
    private Customer customer;
    CustomerApplication customerApp;
    
    public void setCustomer(Customer customer){
        this.customer = customer;
    }
    
    public Customer getCustomer(){
        return customer;
    }
    
    public boolean isShowForm() {
        return showForm;
    }

    public RemoveCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        customerApp = (CustomerApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "customerApplication");
        
        customerApp.updateCustomerList();
        
       
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "customerManagedBean");
    }

 
    public void removeCustomer(int customerId) {
       try
       {
           
    	   customerManagedBean.removeCustomer(customerId);

    	   customerApp.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been deleted succesfully"));     
       }
       catch (Exception ex)
       {
           
       }
       showForm = true;

    }
 
}

