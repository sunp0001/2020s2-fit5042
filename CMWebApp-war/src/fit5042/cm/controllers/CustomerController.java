package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Sunpreet Kaur
 */
@Named(value = "customerController")
@RequestScoped
public class CustomerController {

    private int customerId; 

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    private fit5042.cm.repository.entities.Customer customer;
    

    public CustomerController() {
     
    	customerId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerID"));
        
        customer = getCustomer();
    }

    public fit5042.cm.repository.entities.Customer getCustomer() {
        if (customer == null) {
         
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            CustomerApplication app
                    = (CustomerApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "customerApplication");
          
            return app.getCustomers().get(--customerId); 
        }
        return customer;
    }
}
