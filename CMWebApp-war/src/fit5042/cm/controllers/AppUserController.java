package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Sunpreet Kaur
 */
@Named(value = "appUserController")
@RequestScoped
public class AppUserController {

    private int userId; 

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    private fit5042.cm.repository.entities.AppUser appUser;
    

    public AppUserController() {
       
    	userId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("userID"));
       
    	appUser = getUser();
    }

    public fit5042.cm.repository.entities.AppUser getUser() {
        if (appUser == null) {
        
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            AppUserApplication app
                    = (AppUserApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "userApplication");
   
            return app.getAppUsers().get(--userId); 
        }
        return appUser;
    }
}
