package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Sunpreet Kaur
 */
@Named(value = "industryTypeController")
@RequestScoped
public class IndustryTypeController {

    private int industryTypeId; 

    public int getIndustryTypeId() {
        return industryTypeId;
    }

    public void setIndustryTypeId(int industryTypeId) {
        this.industryTypeId = industryTypeId;
    }
    
    private fit5042.cm.repository.entities.IndustryType industryType;
    

    public IndustryTypeController() {
     
    	industryTypeId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("industryTypeID"));
     
    	industryType = getIndustryType();
    }

    public fit5042.cm.repository.entities.IndustryType getIndustryType() {
        if (industryType == null) {
           
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            IndustryTypeApplication app
                    = (IndustryTypeApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "industryTypeApplication");
       
            return app.getIndustryTypes().get(--industryTypeId); 
        }
        return industryType;
    }
}
