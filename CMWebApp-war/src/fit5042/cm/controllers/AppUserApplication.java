package fit5042.cm.controllers;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import fit5042.cm.mbeans.AppUserManagedBean;
import javax.inject.Named;
import fit5042.cm.repository.entities.AppUser;
import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;


/**
 * 
 *
 * @author Sunpreet Kaur
 */

@Named(value = "appUserApplication")
@ApplicationScoped

public class AppUserApplication {
   
    @ManagedProperty(value="#{appUserManagedBean}") 
    AppUserManagedBean appUserManagedBean;
    
    private ArrayList<AppUser> appUsers;

    private boolean showForm = true;
  
    public boolean isShowForm() {
        return showForm;
    }


    public AppUserApplication() throws Exception {       
    	appUsers = new ArrayList<>();
        

        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        appUserManagedBean = (AppUserManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "appUserManagedBean");
        
        
        updateUserList();
    }

    public ArrayList<AppUser> getAppUsers() {
        return appUsers;
    }
    
    private void setAppUsers(ArrayList<AppUser> newUsers) {
        this.appUsers = newUsers;
    }
    
   
    public void updateUserList()
    {
        if (appUsers != null && appUsers.size() > 0)
        {
            
        }
        else
        {
        	appUsers.clear();

            for (fit5042.cm.repository.entities.AppUser appUser : appUserManagedBean.getAllAppUsers())
            {
            	appUsers.add(appUser);
            }

            setAppUsers(appUsers);
        }
    }
    
    public void searchUserById(int userId)
    {
    	appUsers.clear();
        
    	appUsers.add(appUserManagedBean.searchAppUserById(userId));
    }
    
    public void searchUserByName(String nameOfUser)
    {
    	appUsers.clear();
        
    	appUsers.add(appUserManagedBean.searchAppUserByName(nameOfUser));
    }
    

    public void searchAll()
    {
    	appUsers.clear();
        
        for (fit5042.cm.repository.entities.AppUser appUser : appUserManagedBean.getAllAppUsers())
        {
        	appUsers.add(appUser);
        }
        
        setAppUsers(appUsers);
    }
}
