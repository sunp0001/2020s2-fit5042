package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;



/**
 *
 * @author Sunpreet Kaur */
@RequestScoped
@Named("searchContact")
public class SearchContact {
    private Contact contact;
    
    ContactApplication contactApp;
    
    private int searchByInt;
    private String searchByString;
    private int contactCustomerId;
    

    public ContactApplication getApp() {
        return contactApp;
    }

    public void setApp(ContactApplication contactApp) {
        this.contactApp = contactApp;
    }
   

    public int getContactCustomerId() {
		return contactCustomerId;
	}

	public void setContactCustomerId(int contactCustomerId) {
		this.contactCustomerId = contactCustomerId;
	}

	public String getSearchByString() {
        return searchByString;
    }

    public void setSearchByString(String searchByString) {
        this.searchByString = searchByString;
    }

    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }
    
    public void setContact(Contact contact){
        this.contact = contact;
    }
    
    public Contact getContact(){
        return contact;
    }
    
    public SearchContact() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        contactApp = (ContactApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "contactApplication");
        
        contactApp.updateContactList();
    }

  
    public void searchContactById(int contactId) 
    {
       try
       {
           
    	   contactApp.searchContactById(contactId);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchContactByName(String contactName) 
    {
       try
       {
           
    	   contactApp.searchContactByName(contactName);
       }
       catch (Exception ex)
       {
           
       }
    }
    public void searchContactByCustomerId(int contactCustomerId) 
    {
       try
       {
            
    	   contactApp.searchContactByCustomerId(contactCustomerId);
       }
       catch (Exception ex)
       {
           
       }
    }
    

    
    public void searchAll() 
    {
       try
       {
            
    	   contactApp.searchAll();
       }
       catch (Exception ex)
       {
           
       }
    }
    
}

