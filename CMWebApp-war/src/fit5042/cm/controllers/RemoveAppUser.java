package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.cm.mbeans.AppUserManagedBean;
import javax.faces.bean.ManagedProperty;



/**
 *
 * @author Sunpreet Kaur
 */
@RequestScoped
@Named("removeAppUser")
public class RemoveAppUser {
   @ManagedProperty(value="#{appUserManagedBean}") 
   AppUserManagedBean appUserManagedBean;
    
    private boolean showForm = true;
    private AppUser appUser;
    AppUserApplication userApp;
    
    public void setAppUser(AppUser appUser){
        this.appUser = appUser;
    }
    
    public AppUser getAppUser(){
        return appUser;
    }
    
    public boolean isShowForm() {
        return showForm;
    }

    public RemoveAppUser() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        userApp = (AppUserApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "appUserApplication");
        
        userApp.updateUserList();
        
       
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        appUserManagedBean = (AppUserManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "appUserManagedBean");
    }

  
    public void removeUser(int userId) {
       try
       {
            
    	   appUserManagedBean.removeAppUser(userId);

           
    	   userApp.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User has been deleted succesfully"));     
       }
       catch (Exception ex)
       {
           
       }
       showForm = true;

    }
 
}

