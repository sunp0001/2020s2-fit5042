package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.cm.repository.entities.AppUser;
import fit5042.cm.mbeans.AppUserManagedBean;
import javax.faces.bean.ManagedProperty;


/**
 *
 * @author Sunpreet Kaur
 */
@RequestScoped
@Named("addAppUser")
public class AddAppUser {
    @ManagedProperty(value="#{appUserManagedBean}") 
    AppUserManagedBean appUserManagedBean;
    
    private boolean showForm = true;

    private  AppUser appUser;
    
    AppUserApplication userApp;
    
    public void setAppUser( AppUser appUser){
        this.appUser = appUser;
    }
    
    public  AppUser getAppUser(){
        return appUser;
    }
    
    public boolean isShowForm() {
        return showForm;
    }

    public AddAppUser() 
    {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        userApp  = (AppUserApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "appUserApplication");
        
 
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        appUserManagedBean = (AppUserManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "appUserManagedBean");
    }

    public void addUser(AppUser appUser) {
    
       try
       {
         
    	   appUserManagedBean.addAppUser(appUser);

           
            userApp.searchAll();
            

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User has been added succesfully"));
         
       }
       catch (Exception ex)
       {
           
       }
        showForm = true;
    }
   
}
