package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Sunpreet Kaur
 */
@Named(value = "contactController")
@RequestScoped
public class ContactController {

    private int contactId; 

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }
    
    private fit5042.cm.repository.entities.Contact contact;
    

    public ContactController() {
      
    	contactId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("contactID"));
      
        contact = getContact();
    }

    public fit5042.cm.repository.entities.Contact getContact() {
        if (contact == null) {
            
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            ContactApplication app
                    = (ContactApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "contactApplication");
     
            return app.getContacts().get(--contactId); 
        }
        return contact;
    }
}
