package fit5042.cm.controllers;

import java.util.ArrayList;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import fit5042.cm.mbeans.ContactManagedBean;
import javax.inject.Named;
import fit5042.cm.repository.entities.Contact;
import fit5042.cm.repository.entities.Customer;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;


/**
 
 *
 * @author Sunpreet Kaur
 */

@Named(value = "contactApplication")
@ApplicationScoped

public class ContactApplication {
    
    @ManagedProperty(value="#{contactManagedBean}") 
    ContactManagedBean contactManagedBean;
    
    private ArrayList<Contact> contacts;

    private boolean showForm = true;
  
    public boolean isShowForm() {
        return showForm;
    }


    public ContactApplication() throws Exception {       
    	contacts = new ArrayList<>();
        
      
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "contactManagedBean");
        
        
        updateContactList();
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }
    
    private void setContacts(ArrayList<Contact> newContacts) {
        this.contacts = newContacts;
    }
    
  
    public void updateContactList()
    {
        if (contacts != null && contacts.size() > 0)
        {
            
        }
        else
        {
        	contacts.clear();

            for (fit5042.cm.repository.entities.Contact contact : contactManagedBean.getAllContacts())
            {
            	contacts.add(contact);
            }

            setContacts(contacts);
        }
    }
    
    public void searchContactById(int contactId)
    {
    	contacts.clear();
        
    	contacts.add(contactManagedBean.searchContactById(contactId));
    }
    
    public void searchContactByName(String contactName)
    {
    	contacts.clear();
        
    	for (Contact contact : contactManagedBean.searchContactByName(contactName)) {
    		contacts.add(contact);
        }

        setContacts(contacts);
    }
    
    public void searchContactByCustomerId(int contactCustomerId)
    {
    	contacts.clear();
    	Set<Contact> contactsByCustomer =  contactManagedBean.searchContactByCustomerId(contactCustomerId);
        
        for (fit5042.cm.repository.entities.Contact contact : contactsByCustomer)
        {
        	contacts.add(contact);
        }
        
        setContacts(contacts);
    }

    public void searchAll()
    {
    	contacts.clear();
        
        for (fit5042.cm.repository.entities.Contact contact : contactManagedBean.getAllContacts())
        {
        	contacts.add(contact);
        }
        
        setContacts(contacts);
    }
}
