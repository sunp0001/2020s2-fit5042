package fit5042.cm.controllers;

import fit5042.cm.repository.entities.Customer;

import java.io.Serializable;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;


/**
 *
 * @author Sunpreet Kaur
 */
@RequestScoped
@Named(value = "user")
public class AppUser implements Serializable {
	
	private int id;
	private String name;
	private String PhoneNo;
	private String username;
	private String password;
	
	private Set<Customer> customers;
	
	public AppUser() {
		
	}

	public AppUser(int id, String name, String phoneNo, String username, String password) {
		super();
		this.id = id;
		this.name = name;
		PhoneNo = phoneNo;
		this.username = username;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNo() {
		return PhoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		PhoneNo = phoneNo;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setContacts(Set<Customer> customers) {
		this.customers = customers;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", PhoneNo=" + PhoneNo + ", username=" + username + ", password="
				+ password + ", customers=" + customers + "]";
	}





}
