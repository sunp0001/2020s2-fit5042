package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;



/**
 *
 * @author Sunpreet Kaur */
@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
    private Customer customer;
    
    CustomerApplication customerApp;
    
    private int searchByInt;
    private String searchByString;

    public CustomerApplication getApp() {
        return customerApp;
    }

    public void setApp(CustomerApplication customerApp) {
        this.customerApp = customerApp;
    }


    public String getSearchByString() {
        return searchByString;
    }

    public void setSearchByString(String searchByString) {
        this.searchByString = searchByString;
    }

    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }


    
    public void setCustomer(Customer customer){
        this.customer = customer;
    }
    
    public Customer getCustomer(){
        return customer;
    }
    
    public SearchCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        customerApp = (CustomerApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "customerApplication");
        
        customerApp.updateCustomerList();
    }

 
    public void searchCustomerById(int customerId) 
    {
       try
       {
            
    	   customerApp.searchCustomerById(customerId);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchCustomerByName(String customerName) 
    {
       try
       {
            
    	   customerApp.searchCustomerByName(customerName);
       }
       catch (Exception ex)
       {
           
       }
    }

    
    public void searchAll() 
    {
       try
       {
           
    	   customerApp.searchAll();
       }
       catch (Exception ex)
       {
           
       }
    }
    
}

