package fit5042.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;


import fit5042.cm.mbeans.ContactManagedBean;
import javax.faces.bean.ManagedProperty;


/**
 *
 * @author Sunpreet Kaur
 */
@RequestScoped
@Named("addContact")
public class AddContact {
    @ManagedProperty(value="#{contactManagedBean}") 
    ContactManagedBean contactManagedBean;
    
    private boolean showForm = true;

    private Contact contact ;
    
    ContactApplication contactApp;
    
    public void setContact(Contact contact){
        this.contact = contact;
    }
    
    public Contact getContact(){
        return contact;
    }
    
    public boolean isShowForm() {
        return showForm;
    }

    public AddContact() 
    {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        contactApp  = (ContactApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "contactApplication");
        
      
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "contactManagedBean");
    }

    public void addContact(Contact contact) {
      
       try
       {
            
    	   contactManagedBean.addContact(contact);

            
    	   contactApp.searchAll();
          

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact has been added succesfully"));
         
       }
       catch (Exception ex)
       {
           
       }
        showForm = true;
    }
   
}

