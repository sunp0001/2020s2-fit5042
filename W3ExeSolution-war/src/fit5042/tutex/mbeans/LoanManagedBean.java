/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.mbeans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.enterprise.context.SessionScoped;

import fit5042.tutex.repository.entities.Loan;
import fit5042.tutex.calculator.MonthlyPaymentCalculator;

/**
 *
 * @author: originally created by Eddie. The following code has been changed in
 * order for students to practice.
 */

// Change this managed bean to be instantiated as eager
@ManagedBean(name = "loanManagedBean", eager = true) //If eager = "true" then managed bean is created before it is requested for the first time otherwise "lazy" initialization is used in which bean will be created only when it is requested.
// Set this managed bean as a Session scoped
@SessionScoped
public class LoanManagedBean implements Serializable {

	//This managed bean will now include another @EJB which is the MonthlyPaymentCalculator
    @EJB
    MonthlyPaymentCalculator calculator;
	
	private Loan loan;

    public LoanManagedBean() {
        this.loan = new Loan();

    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    
    // Complete the calculate() method to populate monthlyPayment attribute of the loan instance
    public String calculate() {
    	//You will need to modify the monthlyPayment value and set it as the monthly payment attribute value into the loan instance
        //Please complete this method starts from here
    	
    	// calling calculate function of MonthlyPaymentCalculator class  will return monthly payment
    	// to call this function we will use calculator which is a reference to the object of MonthlyPaymentCalculator class
    	//this is reference to the object of current class which is LoanManagedBean
        double monthlyPayment = this.calculator.calculate(loan.getPrinciple(), loan.getNumberOfYears(), loan.getInterestRate());
        
        // Then pass monthlyPayment as an argument to setMonthlyPayment() method of Loan class
        this.loan.setMonthlyPayment(monthlyPayment);
        return "index";
    }
}
