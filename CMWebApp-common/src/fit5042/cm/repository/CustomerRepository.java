package fit5042.cm.repository;


import java.util.List;

import javax.ejb.Remote;

import fit5042.cm.repository.entities.Customer;
import fit5042.cm.repository.entities.IndustryType;

/**
 * @author Sunpreet Kaur
 */
@Remote
public interface CustomerRepository {
	
	/**
     * Add the customer being passed as parameter into the repository
     *
     * @param customer - the customer to add
     */
    public void addCustomer(Customer customer) throws Exception;

    /**
     * Search for a customer by its customer ID
     *
     * @param id - the customerId of the customer to search for
     * @return the customer found
     */
    public Customer searchCustomerById(int customerId) throws Exception;
    /**
     * Search for a customer by its customer name
     *
     * @param id - the customerName or string containing customer name of the customer to search for
     * @return the customer or customers found
     */
    
    public List<Customer> searchCustomerByName(String name) throws Exception;

    /**
     * Return all the customers in the repository
     *
     * @return all the customers in the repository
     */
    public List<Customer> getAllCustomers() throws Exception;
    
    /**
     * Return all the industry types in the repository
     *
     * @return all the industry types in the repository
     */
    public List<IndustryType> getAllIndustryType() throws Exception;
    
    /**
     * Remove the customer, whose customer ID matches the one being passed as parameter, from the repository
     *
     * @param customerId - the ID of the customer to remove
     */
    public void removeCustomer(int customerId) throws Exception;
    
    /**
     * Update a customer in the repository
     *
     * @param customer - the updated information regarding a customer
     */
    public void editCustomer(Customer customer) throws Exception;

	
    /**
     * Search for customer whose has purchased products greater than or equal to a no. of products purchased
     *
     * @param noOfProductsPurchased - the noOfProductsPurchased that the number of the returned customer has purchased number of products greater than or equal to
     * @return the customers found
     */
    public List<Customer> searchCustomerByNoOfProductPurchased(int noOfProductsPurchased) throws Exception;
    

}
