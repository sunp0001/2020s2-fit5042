package fit5042.cm.repository.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Sunpreet Kaur
 */
@Entity
@Table(name = "CONTACT")
@NamedQueries({ @NamedQuery(name = Contact.GET_ALL_QUERY_NAME, query = "SELECT c FROM Contact c order by c.contactId desc") })
public class Contact implements Serializable {

	public static final String GET_ALL_QUERY_NAME = "Contact.getAll";

	private int contactId;
	private String name;
	private String phoneNo;
	private String email;
	private String role;
	private Date registeredOn;
	private String registeredBy;
	

	private Customer contactCustomer;
	private int contactCustomerId;
	
	

	public Contact() {
	}

	public Contact(int contactId, String name, String phoneNo, String email, String role, Date registeredOn,
			String registeredBy, Customer contactCustomer, int contactCustomerId) {
		super();
		this.contactId = contactId;
		this.name = name;
		this.phoneNo = phoneNo;
		this.email = email;
		this.role = role;
		this.registeredOn = registeredOn;
		this.registeredBy = registeredBy;
		this.contactCustomer = contactCustomer;
		this.contactCustomerId = contactCustomerId;
	}

	public int getContactCustomerId() {
		return contactCustomerId;
	}

	public void setContactCustomerId(int contactCustomerId) {
		this.contactCustomerId = contactCustomerId;
	}

	@Id
	    @GeneratedValue
	    @Column(name = "contact_id")
	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date getRegisteredOn() {
		return registeredOn;
	}

	public void setRegisteredOn(Date registeredOn) {
		this.registeredOn = registeredOn;
	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	@ManyToOne
	public Customer getContactCustomer() {
		return contactCustomer;
	}


	public void setContactCustomer(Customer contactCustomer) {
		this.contactCustomer = contactCustomer;
	}

	@Override
	public String toString() {
		return "Contact [contactId=" + contactId + ", name=" + name + ", phoneNo=" + phoneNo + ", email=" + email
				+ ", role=" + role + ", registeredOn=" + registeredOn + ", registeredBy=" + registeredBy + ", contactCustomer="
				+ contactCustomer + "]";
	}





	

	
	

	
	

}
