package fit5042.cm.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import java.util.Date;

/**
 *
 * @author Sunpreet Kaur
 */

@Entity
@NamedQueries({
	@NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c order by c.customerId desc") })

public class Customer implements Serializable {

	public static final String GET_ALL_QUERY_NAME = "Customer.getAll";

	private int customerId;
	private String name;
	private String phoneNo;
	private String email;
	private int noOfProductsPurchased;
	private Date registeredOn;
	private String registeredBy;

	private Address address;

	private Set<Contact> contacts;

	private IndustryType industryType;
	private String industryTypeName;
	

	private AppUser customerAssignedTo;
	private String customerAssignee;

	public Customer() {

	}



	public Customer(int customerId, String name, String phoneNo, String email, int noOfProductsPurchased,
			Date registeredOn, String registeredBy,Address address,IndustryType industryType, AppUser customerAssignedTo, String industryTypeName,String customerAssignee) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.phoneNo = phoneNo;
		this.email = email;
		this.noOfProductsPurchased = noOfProductsPurchased;
		this.registeredOn = registeredOn;
		this.registeredBy = registeredBy;
		this.address = address;
		this.industryType = industryType;
		this.customerAssignedTo = customerAssignedTo;
		this.industryTypeName = industryTypeName;
		this.customerAssignee = customerAssignee;
		
	}

	@Id
	@GeneratedValue
	@Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}


	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getPhoneNo() {
		return phoneNo;
	}



	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}


	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}


	public int getNoOfProductsPurchased() {
		return noOfProductsPurchased;
	}

	public void setNoOfProductsPurchased(int noOfProductsPurchased) {
		this.noOfProductsPurchased = noOfProductsPurchased;
	}


	public Date getRegisteredOn() {
		return registeredOn;
	}


	public void setRegisteredOn(Date registeredOn) {
		this.registeredOn = registeredOn;
	}


	public String getRegisteredBy() {
		return registeredBy;
	}


	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	@Embedded
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}



	@ManyToOne
	public IndustryType getIndustryType() {
		return industryType;
	}


	public void setIndustryType(IndustryType industryType) {
		this.industryType = industryType;
	}



	@OneToMany(mappedBy = "contactCustomer")
	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}


	@ManyToOne
	public AppUser getCustomerAssignedTo() {
		return customerAssignedTo;
	}

	public void setCustomerAssignedTo(AppUser customerAssignedTo) {
		this.customerAssignedTo = customerAssignedTo;
	}

	

	public String getIndustryTypeName() {
		return industryTypeName;
	}


	public void setIndustryTypeName(String industryTypeName) {
		this.industryTypeName = industryTypeName;
	}


	public String getCustomerAssignee() {
		return customerAssignee;
	}


	public void setCustomerAssignee(String customerAssignee) {
		this.customerAssignee = customerAssignee;
	}


	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", name=" + name + ", phoneNo=" + phoneNo + ", email=" + email
				+ ", noOfProductsPurchased=" + noOfProductsPurchased + ", registeredOn=" + registeredOn
				+ ", registeredBy=" + registeredBy + ", address=" + address + ", contacts=" + contacts
				+ ", industryType=" + industryType + ", industryTypeName=" + industryTypeName + ", customerAssignedTo="
				+ customerAssignedTo + ", customerAssignee=" + customerAssignee + "]";
	}


	


	

}












