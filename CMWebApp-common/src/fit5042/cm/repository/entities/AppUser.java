package fit5042.cm.repository.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
*
* @author Sunpreet Kaur
*/

@Entity
@NamedQueries({
		@NamedQuery(name = AppUser.GET_ALL_QUERY_NAME, query = "SELECT au FROM AppUser au") })


public class AppUser implements Serializable {
	
	public static final String GET_ALL_QUERY_NAME = "AppUser.getAll";
	
	private int id;
	private String name;
	private String phoneNo;
	private String username;
	private String password;
	
	private Set<Customer> customers;
	
	public AppUser() {
		
	}
	

	public AppUser(int id) {
		super();
		this.id = id;
	}


	public AppUser(int id, String name, String phoneNo, String username, String password) {
		super();
		this.id = id;
		this.name = name;
		this.phoneNo = phoneNo;
		this.username = username;
		this.password = password;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "user_id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	@Column(name = "name")
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getPhoneNo() {
		return phoneNo;
	}



	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}



	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    @OneToMany(mappedBy = "customerAssignedTo")
	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setContacts(Set<Customer> customers) {
		this.customers = customers;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", phoneNo=" + phoneNo + ", username=" + username + ", password="
				+ password + ", customers=" + customers + "]";
	}




	

}
