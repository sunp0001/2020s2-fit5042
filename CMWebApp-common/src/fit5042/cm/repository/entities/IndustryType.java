package fit5042.cm.repository.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
*
* @author Sunpreet Kaur
*/

@Entity
@NamedQueries({
		@NamedQuery(name = IndustryType.GET_ALL_QUERY_NAME, query = "SELECT i FROM IndustryType i") })


public class IndustryType implements Serializable {
	
	public static final String GET_ALL_QUERY_NAME = "IndustryType.getAll";
 
	private int industryTypeId;
	private String industryTypeName;
	
	private Set<Customer> customers;

	
	public IndustryType() {
		
	}

	public IndustryType(int industryTypeId, String industryTypeName) {
		super();
		this.industryTypeId = industryTypeId;
		this.industryTypeName = industryTypeName;
	}

	
	@Id
	    @GeneratedValue
	    @Column(name = "industrytype_id")
	public int getIndustryTypeId() {
		return industryTypeId;
	}

	public void setIndustryTypeId(int industryTypeId) {
		this.industryTypeId = industryTypeId;
	}

	public String getIndustryTypeName() {
		return industryTypeName;
	}

	public void setIndustryTypeName(String industryTypeName) {
		this.industryTypeName = industryTypeName;
	}
	
    @OneToMany(mappedBy = "industryType")
	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	@Override
	public String toString() {
		return "IndustryType [industryTypeId=" + industryTypeId + ", industryTypeName=" + industryTypeName
				+ ", customers=" + customers + "]";
	}

	


	
	
}
