package fit5042.cm.repository;

import java.util.List;


import javax.ejb.Remote;

import fit5042.cm.repository.entities.IndustryType;


/**
 * @author Sunpreet Kaur
 */
@Remote
public interface IndustryTypeRepository{

    /**
     * Add the industry type being passed as parameter into the repository
     *
     * @param  industry type - the  industry type to add
     */
    public void addIndustryType(IndustryType industryType) throws Exception;

    /**
     * Search for a  industry type by its  industry type ID
     *
     * @param id - the industryTypeId of the  industry type to search for
     * @return the  industry type found
     */
    public IndustryType searchIndustryTypeById(int industryTypeId) throws Exception;
    /**
     * Search for a industry type by its industry type name
     *
     * @param id - the industryTypeName of the industry type to search for
     * @return the industry type found
     */
    
    public IndustryType searchIndustryTypeByName(String industryTypeName) throws Exception;   
    
    /**
     * Return all the  industry types in the repository
     *
     * @return all the  industry types  in the repository
     */
    public List<IndustryType> getAllIndustryType() throws Exception;
    
    /**
     * Remove the  industry type, whose  industry type ID matches the one being passed as parameter, from the repository
     *
     * @param industryTypeId - the ID of the  industry type to remove
     */
    public void removeIndustryType(int industryTypeId) throws Exception;
    
    /**
     * Update a  industry type in the repository
     *
     * @param  industry type - the updated information regarding a  industry type
     */
    public void editIndustryType(IndustryType industryType) throws Exception;
    
    

}


