package fit5042.cm.repository;

import java.util.List;


import javax.ejb.Remote;

import fit5042.cm.repository.entities.AppUser;


/**
 * @author Sunpreet Kaur
 */
@Remote
public interface AppUserRepository {

    /**
     * Add the user being passed as parameter into the repository
     *
     * @param user - the user to add
     */
    public void addAppUser(AppUser appUser) throws Exception;

    /**
     * Search for a user by its user ID
     *
     * @param id - the userId of the user to search for
     * @return the user found
     */
    public AppUser searchAppUserById(int appUserId) throws Exception;
    
    /**
     * Search for a user by its name
     *
     * @param id - the nameOfUser of the user to search for
     * @return the user found
     */
    
    public AppUser searchAppUserByName(String nameOfUser) throws Exception;

    
    
    /**
     * Return all the users in the repository
     *
     * @return all the users  in the repository
     */
    public List<AppUser> getAllAppUsers() throws Exception;
    
    /**
     * Remove the user, whose user ID matches the one being passed as parameter, from the repository
     *
     * @param userId - the ID of the user to remove
     */
    public void removeAppUser(int appUserId) throws Exception;
    
    /**
     * Update a user in the repository
     *
     * @param user - the updated information regarding a user
     */
    public void editAppUser(AppUser appUser) throws Exception;
    
    

}

