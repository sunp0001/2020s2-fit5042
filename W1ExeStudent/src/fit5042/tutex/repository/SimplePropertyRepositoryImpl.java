package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * This class implements the PropertyRepository class. 
 * 
 * @author Sunpreet Kaur
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository{
	
	private final List<Property> propertiesList;

    public SimplePropertyRepositoryImpl() {
		this.propertiesList = new ArrayList<>();
        
    }

	@Override
	public void addProperty(Property property) throws Exception {
		if((!propertiesList.contains(property)) && searchPropertyById(property.getId()) == null)
			this.propertiesList.add(property);
		else 
			System.out.println("The property you are trying to add already exists.");		
	}

	@Override
	public Property searchPropertyById(int id) throws Exception {
		for (Property property : propertiesList) {
            if (property.getId() == id)
                return property;
        }
        return null;
	}

	@Override
	public List<Property> getAllProperties() throws Exception {
		List<Property> allProperties = new ArrayList<Property>(this.propertiesList.size());
		for(Property property : this.propertiesList) {
			allProperties.add(property);
		}		
		return allProperties;
	}
    
}

