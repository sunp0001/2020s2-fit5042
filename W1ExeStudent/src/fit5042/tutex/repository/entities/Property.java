package fit5042.tutex.repository.entities;

public class Property {
	
	private int id;
	private String address;
	private int numberOfBedrooms;
	private int size;
	private double price;
	
    public Property() {
        this.id = 0;
        this.address = "not known";
        this.numberOfBedrooms = 0;
        this.size = 0;
        this.price = 0.0d;
    }
      
	public Property(int id, String address, int numberOfBedrooms, int size, double price) {
		super();
		this.id = id;
		this.address = address;
		this.numberOfBedrooms = numberOfBedrooms;
		this.size = size;
		this.price = price;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public int getNumberOfBedrooms() {
		return numberOfBedrooms;
	}
	
	public void setNumberOfBedrooms(int numberOfBedrooms) {
		this.numberOfBedrooms = numberOfBedrooms;
	}
	
	public int getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Property id=" + id + ", Property address=" + address + ", Property Number of bedrooms=" + numberOfBedrooms + ", Property Size="
				+ size + ", Property price=" + price ;
	}
	
	
    
}
