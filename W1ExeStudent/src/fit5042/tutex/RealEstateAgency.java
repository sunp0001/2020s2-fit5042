package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * This is the main driver class. 
 * 
 * @author Sunpreet Kaur
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	
        try {
               this.propertyRepository.addProperty(new Property(1, "24 Boston Ave, Malvern East VIC 3145, Australia", 2, 150, 420000.00));
               this.propertyRepository.addProperty(new Property(2, "11 Bettina St, Clayton VIC 3168, Australia", 3, 352, 360000.00));
               this.propertyRepository.addProperty(new Property(3, "3 Wattle Ave, Glen Huntly VIC 3163, Australia", 5, 800, 650000.00));
               this.propertyRepository.addProperty(new Property(4, "3 Hamilton St, Bentleigh VIC 3204, Australia", 2, 170, 435000.00));
               this.propertyRepository.addProperty(new Property(5, "82 Spring Rd, Hampton East VIC 3188, Australia", 1, 60, 820000.00));
               System.out.println("5 properties added successfully");
        } catch (Exception e) {
               System.out.println("An error occured while inserting");
        }
        
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
    	try {
    		List<Property> propertiesList = propertyRepository.getAllProperties();
            for (Property property : propertiesList) {
                System.out.println(property.toString());
            }
		} catch (Exception e) {
			System.out.println("There are no properties in property list");
		}
        
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
    	 Scanner sc = new Scanner(System.in);
         System.out.print("Enter the ID of the property you want to search: ");
         try {
             int propertyID = sc.nextInt();
             Property property = this.propertyRepository.searchPropertyById(propertyID);
             if (property != null) {
                 System.out.println(property.toString());
             } else {
                 System.out.println("Property does not exist.");
             }
         } catch (Exception e) {
             System.out.println("Please enter an integer: " + e.getMessage());
         }
        
    }
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception e) {
            System.out.println("An error occured while running the application: " + e.getMessage());
        }
    }
}
