package fit5042.cm.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.cm.repository.entities.AppUser;

public class JPAUserRepositoryImpl implements AppUserRepository {
	
	@PersistenceContext(unitName = "CMWebApp-ejb")
    private EntityManager entityManager;

	@Override
	public void addAppUser(AppUser appUser) throws Exception {
		List<AppUser> appUsers =  entityManager.createNamedQuery(AppUser.GET_ALL_QUERY_NAME).getResultList(); 
		appUser.setId(appUsers.get(0).getId()+1);
        entityManager.persist(appUser);
		
	}

	@Override
	public AppUser searchAppUserById(int appUserId) throws Exception {
		AppUser user = entityManager.find(AppUser.class, appUserId);
        return user;
	}

	@Override
	public AppUser searchAppUserByName(String nameOfUser) throws Exception {
		AppUser appUser = entityManager.find(AppUser.class, nameOfUser);
        return appUser;
	}

	@Override
	public List<AppUser> getAllAppUsers() throws Exception {
		 return entityManager.createNamedQuery(AppUser.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void removeAppUser(int appUserId) throws Exception {
		AppUser appUser = this.searchAppUserById(appUserId);

        if (appUser != null) {
            entityManager.remove(appUser);
        }
		
	}

	@Override
	public void editAppUser(AppUser appUser) throws Exception {
		try {
            entityManager.merge(appUser);
        } catch (Exception ex) {
            
        }
	
		
	}

}
