package fit5042.cm.repository;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fit5042.cm.repository.entities.Contact;
import fit5042.cm.repository.entities.Customer;


/**
 *
 * @author Sunpreet Kaur
 */
@Stateless
public class JPAContactRepositoryImpl implements ContactRepository {

	 @PersistenceContext(unitName = "CMWebApp-ejb")
	    private EntityManager entityManager;

	    @Override
	    public void addContact(Contact contact) throws Exception {
	    	List<Contact> contacts =  entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList(); 
	    	contact.setContactId(contacts.get(0).getContactId() + 1);
	        entityManager.persist(contact);
	    }
	     
	    @Override
	    public Contact searchContactById(int id) throws Exception {
	    	Contact contact = entityManager.find(Contact.class, id);
	        return contact;
	    }
	    @Override
	    public List<Contact> searchContactByName(String name) throws Exception {
	    	List<Contact> contactList = new ArrayList<>();
	    	
	    	List<Contact> contacts =  entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList(); 
			
			for (Contact c : contacts) {
				if (c.getName().contains(name)){
					contactList.add(c);
				}
			}
			return contactList;
		}
	    @Override
	    public List<Contact> getAllContacts() throws Exception {
	        return entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();
	    }

	    @Override
	    public void removeContact(int contactId) throws Exception {
	    	Contact contact = this.searchContactById(contactId);

	        if (contact != null) {
	            entityManager.remove(contact);
	        }
	    }

	    @Override
	    public void editContact(Contact contact) throws Exception {   
	         try {
	            entityManager.merge(contact);
	        } catch (Exception ex) {
	            
	        }
	    }

	    @Override
	    public Set<Contact> searchContactByCustomer(Customer customer) throws Exception {
	    	customer = entityManager.find(Customer.class, customer.getCustomerId());
	    	customer.getContacts().size();
	        entityManager.refresh(customer);
	        return customer.getContacts();
	    }

}
