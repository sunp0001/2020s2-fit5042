package fit5042.cm.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.cm.repository.entities.IndustryType;

public class JPAIndustryTypeRepositoryImpl  implements IndustryTypeRepository{
	
	@PersistenceContext(unitName = "CMWebApp-ejb")
    private EntityManager entityManager;

	@Override
	public void addIndustryType(IndustryType industryType) throws Exception {
		List<IndustryType> industryTypeList =  entityManager.createNamedQuery(IndustryType.GET_ALL_QUERY_NAME).getResultList(); 
		industryType.setIndustryTypeId(industryTypeList.get(0).getIndustryTypeId() + 1);
        entityManager.persist(industryType);
		
	}

	@Override
	public IndustryType searchIndustryTypeById(int industryTypeId) throws Exception {
		IndustryType industryType = entityManager.find(IndustryType.class, industryTypeId);
        return industryType;
	}

	@Override
	public IndustryType searchIndustryTypeByName(String industryTypeName) throws Exception {
		IndustryType industryType = entityManager.find(IndustryType.class, industryTypeName);
        return industryType;
	}

	@Override
	public List<IndustryType> getAllIndustryType() throws Exception {
		 return entityManager.createNamedQuery(IndustryType.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void removeIndustryType(int industryTypeId) throws Exception {
		IndustryType industryType = this.searchIndustryTypeById(industryTypeId);

        if (industryType != null) {
            entityManager.remove(industryType);
        }
		
	}

	@Override
	public void editIndustryType(IndustryType industryType) throws Exception {
		 try {
	            entityManager.merge(industryType);
	        } catch (Exception ex) {
	            
	        }
		
	}

}
