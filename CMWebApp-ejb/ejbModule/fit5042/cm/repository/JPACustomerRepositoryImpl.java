package fit5042.cm.repository;


import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fit5042.cm.repository.entities.Customer;
import fit5042.cm.repository.entities.IndustryType;


/**
 *
 * @author Sunpreet Kaur
 */
@Stateless
public class JPACustomerRepositoryImpl implements CustomerRepository{


    @PersistenceContext(unitName = "CMWebApp-ejb")
    private EntityManager entityManager;


    @Override
    public void addCustomer(Customer customer) throws Exception {
        List<Customer> customers =  entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList(); 
        customer.setCustomerId(customers.get(0).getCustomerId() + 1);
        entityManager.persist(customer);
       
    }
     
    @Override
    public Customer searchCustomerById(int id) throws Exception {
        Customer customer = entityManager.find(Customer.class, id);
        
        return customer;
    }

    @Override
    public List<Customer> getAllCustomers() throws Exception {
        return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
    }
  
    @Override
	public List<IndustryType> getAllIndustryType() throws Exception {
		 return entityManager.createNamedQuery(IndustryType.GET_ALL_QUERY_NAME).getResultList();
	}
    @Override
    public void removeCustomer(int customerId) throws Exception {
        Customer customer = this.searchCustomerById(customerId);

        if (customer != null) {
            entityManager.remove(customer);
        }
    }

    @Override
    public void editCustomer(Customer customer) throws Exception {   
         try {
            entityManager.merge(customer);
        } catch (Exception ex) {
            
        }
    }


    @Override
    public List<Customer> searchCustomerByName(String name) throws Exception {
    	List<Customer> customerList = new ArrayList<>();
    	
    	List<Customer> customers =  entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList(); 
		
		for (Customer c : customers) {
			if (c.getName().contains(name)) {
				customerList.add(c);
			}
		}
		
		return customerList;

    }
  

	@Override
	public List<Customer> searchCustomerByNoOfProductPurchased(int noOfProductsPurchased) throws Exception {
		   CriteriaBuilder builder = entityManager.getCriteriaBuilder();
	        CriteriaQuery query = builder.createQuery(Customer.class);
	        Root<Customer> c = query.from(Customer.class);
	        query.select(c).where(builder.ge(c.get("noOfProductsPurchased").as(Integer.class), noOfProductsPurchased));
	        List<Customer> customersList = entityManager.createQuery(query).getResultList();
	        return customersList;
	}
}
