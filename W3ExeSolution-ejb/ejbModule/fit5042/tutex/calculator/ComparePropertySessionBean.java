package fit5042.tutex.calculator;

import fit5042.tutex.repository.constants.CommonInstance;
import fit5042.tutex.repository.entities.Property;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.CreateException;
import javax.ejb.Stateful;
//import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateful

public class ComparePropertySessionBean implements CompareProperty {

	// Set is an interface
	
	private Set<Property> set;
    
 

    public ComparePropertySessionBean() {
    	set = new HashSet<>();// HashSet is a class which implements the Set interface
    }
    
    // this method will add property object of class Property if it does not exist in the set
    @Override
    public void addProperty(Property property) {
    	set.add(property);
    }
    
    
    // this method will remove property object of class Property from set by first checking id of the property to be removed with id of each 
    // property that exists in the set
    @Override
    public void removeProperty(Property property) {
        for (Property p : set) {
        	if (p.getPropertyId() == property.getPropertyId()) {
        		set.remove(p);
        		break;
        	}
        }
    }    

    
    //
    
    @Override
    public int bestPerRoom() {
        Integer bestID=0; // initialize bestID with a value 0 where bestID is the id of the property which is the best among all properties
        int numberOfRooms; //  number of rooms in the property
        double price;// price of the property
        double bestPerRoom=100000000.00; // initialize bestPerRoom with a large value 
        
        // continue this till we find the property id of the property with minimum bestPerRoom value
        // for each property in the set 
        for(Property p : set)
        {
            numberOfRooms = p.getNumberOfBedrooms(); //returns the number of bedrooms in that property
            price = p.getPrice();  //returns the price of that property
            
            if(price / numberOfRooms < bestPerRoom)// if  price of that property/the number of bedrooms in that property is less than value of bestPerRoom
            {
                bestPerRoom = price / numberOfRooms; // assign result of price of that property/the number of bedrooms in that property to bestPerRoom
                bestID = p.getPropertyId(); // returns property id of that property
            }
        }
        return bestID;// return the property id of the property with minimum bestPerRoom value
    }

    /**
     *
     * @return 
     * @throws javax.ejb.CreateException
     * @throws java.rmi.RemoteException
     */
    @PostConstruct
    public void init() {
    	set=new HashSet<>();// initialize set
    }

    public CompareProperty create() throws CreateException, RemoteException {
        return null;
    }

    public void ejbCreate() throws CreateException {
    }

}
